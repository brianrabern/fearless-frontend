import Nav from './Nav';
import MainPage from './MainPage';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeConferenceForm from './AttendeeConferenceForm';
import PresentationForm from './PresentationForm';
import {BrowserRouter, Routes, Route } from "react-router-dom";


function App(props) {
  // if (props.attendees === undefined) {
  //   return null;
  // }
  return (
    <>

    <BrowserRouter>
    <Nav/>
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="conference/new" element={<ConferenceForm />}/>
        <Route path="location/new" element={<LocationForm />}/>
        <Route path="attendees/new" element={<AttendeeConferenceForm />}/>
        <Route path="presentations/new" element={<PresentationForm />}/>
        <Route path="attendees" element={<AttendeesList attendees={props.attendees}/>}/>
      </Routes>
    </BrowserRouter>


    </>

  );
}

export default App;
